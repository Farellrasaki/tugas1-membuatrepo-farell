// Soal 1
for(var angka = 0; angka < 10; angka++) {
    console.log('Iterasi ke-' + angka);
  } 

// Soal 2
var angkaGanjil = [1, 3, 5, 7, 9];
var jumlah = 0;
for(var i = 0; i < angkaGanjil.length; i++) {
  jumlah += angkaGanjil[i];
  console.log('Angka Ganjil: ' + angkaGanjil[i] + '' );
}

// Soal 3
for(var deret = 0; deret < 10; deret += 2) {
  console.log('angka Genap: ' + deret);
}
 
// Soal 4
let array = [1,2,3,4,5,6];
console.log(array[5]);

// Soal 5
let array2 = [5,2,4,1,3,5];
array2.sort();
console.log(array2);

// Soal 6
let array3 = ["selamat", "anda", "melakukan", "perulangan", "array", "dengan", "for"];

for(let i = 0; i < array3.length; i++) {
  console.log(array3[i]);
}

// Soal 7
let array4 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

for(let i = 0; i < array4.length; i++) {
  if(array4[i] % 2 === 0) {
    console.log(array4[i]);
  }
}

// Soal 8
let kalimat = ["saya", "sangat", "senang", "belajar", "javascript"];
let kalimatGabung = kalimat.join(" ");
console.log(kalimatGabung);


// Soal 9
var sayuran = [];

sayuran.push("Kangkung");
sayuran.push("Bayam");
sayuran.push("Buncis");
sayuran.push("Kubis");
sayuran.push("Timun");
sayuran.push("Seledri");
sayuran.push("Tauge");

console.log(sayuran);




