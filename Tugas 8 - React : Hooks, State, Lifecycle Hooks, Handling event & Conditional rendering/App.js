import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  const [counter, setCounter] = useState(0);

  const handleClick = () => {
    if (counter >= 10) {
      alert("State count sudah lebih dari 10 !!");
    } else {
      setCounter(counter + 1);
    }
  }

  return (
    <div className="App">
      <header className="shadow-box">
        <h1>Hi, Saya Farell</h1>
        <hr />
        <p>Saya dari batch 44 sudah berhasil menginstall React</p>
      </header>
      <main>
        <br />
        <section className="shadow-box">
          <h3>Data diri peserta kelas Reactjs</h3>
          <hr />
          <ul>
            <li><b>Nama Lengkap :</b> Farell muhammad rasaki</li>
            <li><b>Email :</b> razakymuhammad12@gmail.com</li>
            <li><b>Batch pelatihan :</b> Batch 44</li>
          </ul>
        </section>
      </main>

      <main>
        <br />
        <section className="shadow-box-low">
          <ul>
          </ul>
          <p> {counter}</p>
          <button 
            onClick={handleClick}
            style={{ width: '600px', padding: '5px', fontSize: '1.2rem' }}
          >
            tambah
          </button>
        </section>
      </main>
    </div>
  );
}

export default App;
